package com.yohnk.tcpclone;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws IOException {
        final ExecutorService executorService = Executors.newSingleThreadExecutor();

        final Proxy p = new Proxy();
        executorService.submit(p);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                p.kill();
                executorService.shutdown();
            }

        });
    }


}
