package com.yohnk.tcpclone;

import org.apache.commons.io.output.CountingOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

public class SizeLoggingOutputStream extends CountingOutputStream implements Runnable {
    private static final int pause = 3000;

    private AtomicBoolean running;
    private String name;

    public SizeLoggingOutputStream(OutputStream out, String name) {
        super(out);
        this.running = new AtomicBoolean(false);
        this.name = name;
    }


    public void run() {
        running.set(true);
        while (running.get() && !Thread.interrupted()) {
            System.out.println(name + ": " + getByteCount() + " bytes");
            try {
                Thread.sleep(pause);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void close() throws IOException {
        running.set(false);
        super.close();
    }
}
