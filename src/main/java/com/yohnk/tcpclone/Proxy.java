package com.yohnk.tcpclone;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.TeeInputStream;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Proxy implements Runnable {
    final static int LOCAL_PORT = 100;
    final static String HOST = "192.168.1.1";
    final static int HOST_PORT = 101;


    private AtomicBoolean running;
    private ServerSocket serverSocket;
    private ExecutorService threadPool;


    public Proxy() throws IOException {
        running = new AtomicBoolean(false);
        serverSocket = new ServerSocket(LOCAL_PORT);
        threadPool = Executors.newFixedThreadPool(50);
    }

    public void run() {
        running.set(true);
        while (running.get()) {
            try {
                Socket incomingSocket = serverSocket.accept();
                if (incomingSocket != null) {
                    threadPool.submit(new Worker(incomingSocket));
                }
            } catch (IOException e) {
            }
        }
    }

    public void kill() {
        running.set(false);
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class Worker implements Runnable {
        private Socket incomingSocket;

        public Worker(Socket incomingSocket) {
            this.incomingSocket = incomingSocket;
        }

        public void run() {
            try {
                final AtomicInteger callbacks = new AtomicInteger(0);
                final Socket outgoingSocket = new Socket(HOST, HOST_PORT);

                final long time = System.currentTimeMillis() / 1000;

                System.out.println("Connection Established: " + time + " - " + outgoingSocket.toString());

                TeeInputStream copyIn = new TeeInputStream(incomingSocket.getInputStream(), getCopyStream(time + ".request"));
                TeeInputStream copyOut = new TeeInputStream(outgoingSocket.getInputStream(), getCopyStream(time + ".response"));

                Callable<Void> callback = new Callable<Void>() {
                    public Void call() throws Exception {
                        if (callbacks.incrementAndGet() == 2) {
                            try {
                                outgoingSocket.close();
                            } catch (IOException e) {
                            }
                            try {
                                incomingSocket.close();
                            } catch (IOException e) {
                            }

                            System.out.println("Connection Closed: " + time + " - " + outgoingSocket.toString());

                        }
                        return null;
                    }
                };

                threadPool.submit(new Copier(copyIn, outgoingSocket.getOutputStream(), callback));
                threadPool.submit(new Copier(copyOut, incomingSocket.getOutputStream(), callback));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private OutputStream getCopyStream(String name) throws FileNotFoundException {
        SizeLoggingOutputStream os = new SizeLoggingOutputStream(new FileOutputStream("/tmp/" + name), name);
        threadPool.submit(os);
        return os;
    }

    private class Copier implements Runnable {

        private InputStream in;
        private OutputStream out;
        private Callable<Void> callback;

        public Copier(InputStream in, OutputStream out, Callable<Void> callback) {
            this.in = in;
            this.out = out;
            this.callback = callback;
        }

        public void run() {
            try {
                IOUtils.copyLarge(in, out);
                in.close();
                out.close();
            } catch (IOException e) {
            } finally {
                try {
                    callback.call();
                } catch (Exception e) {
                }
            }
        }

    }

}
